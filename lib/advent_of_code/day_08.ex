defmodule AdventOfCode.Day08 do
  def parse_input(input) do
    input
    |> Enum.reduce_while(nil, fn
      line, nil ->
        line
        |> String.trim()
        |> String.graphemes()
        |> Stream.cycle()
        |> then(&({:cont, {&1, %{}}}))
      "\n", acc -> {:cont, acc}
      line, {instructions, map} ->
        [_, elem, left, right] = Regex.run(~r"^([^ ]+) = \(([^ ]+), ([^ ]+)\)\n?$", line)
        {:cont, {instructions, Map.put(map, elem, {left, right})}}
    end)
  end

  def next_node("L", {left, _}), do: left
  def next_node("R", {_, right}), do: right

  def steps_to_exit({instructions, map}) do
    instructions
    |> Enum.reduce_while({"AAA", 0}, fn
      _, {"ZZZ", nb_steps} -> {:halt, nb_steps}
      instruction, {current_node, nb_steps} -> {:cont, {next_node(instruction, Map.get(map, current_node)), nb_steps+1}}
    end)
  end

  def part1(input) do
    input
    |> parse_input()
    |> steps_to_exit()
  end

  def find_sync_exit(steps_to_exit), do: find_sync_exit(steps_to_exit, Enum.to_list(steps_to_exit) |> Enum.with_index())
  def find_sync_exit(_, [{nb_steps, _} | _]) when nb_steps > 2000000000, do: nil
  def find_sync_exit(_, [{s, _}, {s, _}, {s, _}, {s, _}, {s, _}, {s, _}]), do: s

  def find_sync_exit(steps_to_exit, total_steps_to_exit) do
    next_steps = total_steps_to_exit
    |> Enum.sort_by(fn {v, _} -> v end)
    |> Enum.reduce([], fn
      {v, idx}, [] -> [{v + Enum.at(steps_to_exit, idx), idx}]
      v, acc -> [v | acc]
    end)
    find_sync_exit(steps_to_exit, next_steps)
  end

  def ghost_steps_to_exit({instructions, map}) do
    map
    |> Map.keys()
    |> Enum.filter(&(String.ends_with?(&1, "A")))
    |> IO.inspect()
    |> Enum.map(fn node ->
      instructions
      |> Enum.reduce_while({node, 0}, fn
        instruction, {current_node, nb_steps} ->
          if String.ends_with?(current_node, "Z") do
            {:halt, nb_steps}
          else
            {:cont, {next_node(instruction, Map.get(map, current_node)), nb_steps+1}}
          end
      end)
    end)
    |> IO.inspect()
    |> find_sync_exit()
  end

  def part2(input) do
    input
    |> parse_input()
    |> ghost_steps_to_exit()
  end
end

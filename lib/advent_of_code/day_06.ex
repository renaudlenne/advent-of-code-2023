defmodule AdventOfCode.Day06 do

  def parse_input(input) do
    input
    |> Enum.reduce_while(nil, fn
      line, nil ->
        [_, times, _] = Regex.run(~r"^Time\:(( +\d+)+)\n?$", line)
        {:cont, String.split(times) |> Enum.map(&String.to_integer/1)}
      line, times ->
        [_, distances, _] = Regex.run(~r"^Distance\:(( +\d+)+)\n?$", line)
        {:halt, Enum.zip(times, String.split(distances) |> Enum.map(&String.to_integer/1))}
    end)
  end

  def distance_in_time(press_time, total_time) do
    race_time = total_time - press_time
    speed = press_time
    race_time * speed
  end

  def search_first_beat(_, t, t), do: t
  def search_first_beat({time, record}, min, max) when max === min + 1 do
    case distance_in_time(min, time) do
      d when d > record -> min
      _ -> max
    end
  end
  def search_first_beat({time, record} = race, min, max) do
    mid_point = min + div(max-min, 2)
    case distance_in_time(mid_point, time) do
      d when d > record -> search_first_beat(race, min, mid_point)
      _ -> search_first_beat(race, mid_point, max)
    end
  end

  def search_last_beat(_, t, t), do: t
  def search_last_beat({time, record}, min, max) when max === min + 1 do
    case distance_in_time(min, time) do
      d when d > record -> min
      _ -> max
    end
  end
  def search_last_beat({time, record} = race, min, max) do
    mid_point = min + div(max-min, 2)
    case distance_in_time(mid_point, time) do
      d when d > record -> search_last_beat(race, mid_point, max)
      _ -> search_last_beat(race, min, mid_point)
    end
  end

  def search_beating_time({time, _} = race) do
    search_beating_time(race, div(time, 2))
  end
  def search_beating_time(_, 1), do: nil
  def search_beating_time({time, _}, t) when t >= (time-1), do: nil
  def search_beating_time({time, record} = race, press_time) do
    case distance_in_time(press_time, time) do
      d when d > record -> press_time
      _ ->
        search_beating_time(race, div(press_time, 2)) || search_beating_time(race, press_time*2)
    end
  end


  def how_many_beat_record?({time, _} = race) do
    initial_point = search_beating_time(race)
    first_beat = search_first_beat(race, 1, initial_point)
    last_beat = search_last_beat(race, initial_point, time-1)
    last_beat - first_beat + 1
  end

  def part1(input) do
    parse_input(input)
    |> Enum.map(&how_many_beat_record?/1)
    |> Enum.product()
  end

  def part2(input) do
    input
    |> Enum.reduce_while(nil, fn
      line, nil ->
        [_, times, _] = Regex.run(~r"^Time\:(( +\d+)+)\n?$", line)
        {:cont, String.replace(times, ~r" +", "") |> String.to_integer()}
      line, time ->
        [_, distances, _] = Regex.run(~r"^Distance\:(( +\d+)+)\n?$", line)
        {:halt, {time, String.replace(distances, ~r" +", "") |> String.to_integer()}}
    end)
    |> how_many_beat_record?()
  end
end

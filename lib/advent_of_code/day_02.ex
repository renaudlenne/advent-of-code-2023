defmodule AdventOfCode.Day02 do

  def get_min_amounts(line) do
    [_, id, picks] = Regex.run(~r"^Game (\d+):(.+)\n?$", line)
    min_amounts = picks
      |> String.split(";")
      |> Enum.flat_map(fn pick ->
        pick
        |> String.split(",")
        |> Enum.map(fn color_pick ->
          [_, amount, color] = Regex.run(~r"^ *(\d+) ([a-z]+) *$", color_pick)
          {color, String.to_integer(amount)}
        end)
      end)
      |> Enum.reduce(%{}, fn {color, amount}, min_amounts ->
        case Map.get(min_amounts, color) do
          nil -> Map.put(min_amounts, color, amount)
          v when v < amount -> Map.put(min_amounts, color, amount)
          _ -> min_amounts
        end
      end)
    {String.to_integer(id), min_amounts}
  end

  def part1(input) do
    input
    |> Stream.map(&get_min_amounts/1)
    |> Stream.map(fn {id, min_amounts} ->
      if Map.get(min_amounts, "red", 0) > 12 or Map.get(min_amounts, "green", 0) > 13 or Map.get(min_amounts, "blue", 0) > 14 do
        0
      else
        id
      end
    end)
    |> Enum.sum()
  end

  def part2(input) do
    input
    |> Stream.map(&get_min_amounts/1)
    |> Stream.map(fn {_, min_amounts} -> Map.values(min_amounts) |> Enum.product() end)
    |> Enum.sum()
  end
end

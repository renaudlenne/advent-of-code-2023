defmodule AdventOfCode.Day07 do
  defmodule Bid do
    defstruct [:cards, :hand_type, :bid]
  end

  def get_hand_type(cards) do
    cards
    |> Enum.frequencies()
    |> Map.values()
    |> Enum.sort(&(&1 >= &2))
    |> hand_type_for_frequency()
  end

  @hand_type_order %{:five_of_a_kind => 0, :four_of_a_kind => 1, :full_house => 2, :three_of_a_kind => 3, :two_pairs => 4, :one_pair => 5, :high_card => 6}
  @cards_order %{"A" => 0, "K" => 1, "Q" => 2, "J" => 3, "T" => 4, "9" => 5, "8" => 6, "7" => 7, "6" => 8, "5" => 9, "4" => 10, "3" => 11, "2" => 12}

  def hand_type_for_frequency([5]), do: :five_of_a_kind
  def hand_type_for_frequency([4,1]), do: :four_of_a_kind
  def hand_type_for_frequency([3,2]), do: :full_house
  def hand_type_for_frequency([3,1,1]), do: :three_of_a_kind
  def hand_type_for_frequency([2,2,1]), do: :two_pairs
  def hand_type_for_frequency([2,1,1,1]), do: :one_pair
  def hand_type_for_frequency([1,1,1,1,1]), do: :high_card

  def parse_bid(line) do
    [_, hand, bid] = Regex.run(~r"^([AKQJT2-9]{5}+) (\d+)\n?$", line)
    cards = String.graphemes(hand)
    %Bid{cards: cards, bid: String.to_integer(bid), hand_type: get_hand_type(cards)}
  end

  def compare_next_card([c | rest1], [c | rest2]), do: compare_next_card(rest1, rest2)
  def compare_next_card([c1 | _], [c2 | _]) do
    @cards_order[c1] > @cards_order[c2]
  end

  def compare_cards(%Bid{hand_type: h, cards: cards1}, %Bid{hand_type: h, cards: cards2}) do
    compare_next_card(cards1, cards2)
  end
  def compare_cards(%Bid{hand_type: hand_type1}, %Bid{hand_type: hand_type2}) do
    @hand_type_order[hand_type1] > @hand_type_order[hand_type2]
  end

  def part1(input) do
    input
    |> Stream.map(&parse_bid/1)
    |> Enum.sort(&compare_cards/2)
    |> Enum.with_index()
    |> Enum.map(fn {%Bid{bid: bid}, idx} -> bid*(idx+1) end)
    |> Enum.sum()
  end

  @non_joker_cards ["A", "K", "Q", "T", "9", "8", "7", "6", "5", "4", "3", "2"]
  @cards_order_wild %{"A" => 0, "K" => 1, "Q" => 2, "T" => 3, "9" => 4, "8" => 5, "7" => 6, "6" => 7, "5" => 8, "4" => 9, "3" => 10, "2" => 11, "J" => 12}

  def get_hand_type_wild(cards) do
    if String.contains?(cards, "J") do
      @non_joker_cards
      |> Enum.map(fn joker_value -> get_hand_type(String.replace(cards, "J", joker_value)) end)
      |> Enum.sort(&(@hand_type_order[&1] < @hand_type_order[&2]))
      |> hd()
    else
      get_hand_type(cards)
    end
  end

  def parse_bid_wild(line) do
    [_, cards, bid] = Regex.run(~r"^([AKQJT2-9]{5}+) (\d+)\n?$", line)
    %Bid{cards: cards, bid: String.to_integer(bid), hand_type: get_hand_type_wild(cards)}
  end


  def compare_next_card_wild([c | rest1], [c | rest2]), do: compare_next_card_wild(rest1, rest2)
  def compare_next_card_wild([c1 | _], [c2 | _]) do
    @cards_order_wild[c1] > @cards_order_wild[c2]
  end

  def compare_cards_wild(%Bid{hand_type: h, cards: cards1}, %Bid{hand_type: h, cards: cards2}) do
    compare_next_card_wild(String.graphemes(cards1), String.graphemes(cards2))
  end
  def compare_cards_wild(%Bid{hand_type: hand_type1}, %Bid{hand_type: hand_type2}) do
    @hand_type_order[hand_type1] > @hand_type_order[hand_type2]
  end

  def part2(input) do
    input
    |> Stream.map(&parse_bid_wild/1)
    |> Enum.sort(&compare_cards_wild/2)
    |> Enum.with_index()
    |> Enum.map(fn {%Bid{bid: bid}, idx} -> bid*(idx+1) end)
    |> Enum.sum()
  end
end

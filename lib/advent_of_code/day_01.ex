defmodule AdventOfCode.Day01 do

  def part1(input) do
    input
    |> Stream.map(&String.trim/1)
    |> Stream.map(&String.graphemes/1)
    |> Stream.map(fn line ->
      line
      |> Enum.reduce([], fn grapheme, acc ->
        case (Integer.parse(grapheme)) do
          {v, _} -> [v | acc]
          :error -> acc
        end
      end)
      |> then(fn [first | rest] -> [first, List.last(rest, first)] end)
      |> Enum.reverse()
      |> Integer.undigits()
    end)
    |> Enum.sum()
  end

  def part2(input) do
    input
    |> Stream.map(&String.trim/1)
    |> Stream.map(fn line ->
      line
      |> String.replace("one", "o1e")
      |> String.replace("two", "t2o")
      |> String.replace("three", "t3e")
      |> String.replace("four", "f4r")
      |> String.replace("five", "f5e")
      |> String.replace("six", "s6x")
      |> String.replace("seven", "s7n")
      |> String.replace("eight", "e8t")
      |> String.replace("nine", "n9e")
    end)
    |> Stream.map(&String.graphemes/1)
    |> Stream.map(fn line ->
      line
      |> Enum.reduce([], fn grapheme, acc ->
        case (Integer.parse(grapheme)) do
          {v, _} -> [v | acc]
          :error -> acc
        end
      end)
      |> then(fn [first | rest] -> [first, List.last(rest, first)] end)
      |> Enum.reverse()
      |> Integer.undigits()
    end)
    |> Enum.sum()
  end
end

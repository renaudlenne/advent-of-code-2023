defmodule AdventOfCode.Day03 do
  @num_graphemes ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
  defmodule Part do
    defstruct [:line, :start_x, :length, :value]
  end
  defmodule Map do
    defstruct [:height, :width, :content]
  end

  def create_part(y, start_x, end_x, values) do
    %Part{line: y, start_x: start_x, length: end_x-start_x, value: Enum.reverse(values) |> Enum.join() |> String.to_integer() }
  end

  def find_parts(%Map{content: map_content} = map) do
    map_content
    |> Enum.with_index()
    |> Enum.flat_map(fn {line, y} ->
      line
      |> Enum.with_index()
      |> find_parts_in_line(y)
    end)
    |> Enum.filter(fn part -> is_real_part?(part, map) end)
  end

  def find_parts_in_line(line, y), do: find_part_start(line, [], length(line), y)

  def find_part_start([], prev_parts, _, _), do: prev_parts
  def find_part_start([{v, x} | rest], prev_parts, max_x, y) when v in @num_graphemes, do: find_part_end(rest, x, [v], prev_parts, max_x, y)
  def find_part_start([_ | rest], prev_parts, max_x, y), do: find_part_start(rest, prev_parts, max_x, y)

  def find_part_end([], start_x, values, prev_parts, max_x, y), do: [create_part(y, start_x, max_x, values) | prev_parts]
  def find_part_end([{v, _} | rest], start_x, values, prev_parts, max_x, y) when v in @num_graphemes, do: find_part_end(rest, start_x, [v | values], prev_parts, max_x, y)
  def find_part_end([{_, x} | rest], start_x, values, prev_parts, max_x, y), do: find_part_start(rest, [create_part(y, start_x, x, values) | prev_parts], max_x, y)

  def neighbors(%Part{line: line, start_x: start_x, length: length}) do
    [{line, start_x-1}, {line, start_x+length}] ++
    (start_x-1..(start_x+length)
    |> Enum.flat_map(fn x -> [{line-1, x}, {line+1, x}] end))
  end

  def is_symbol?({-1, _}, _), do: false
  def is_symbol?({_, -1}, _), do: false
  def is_symbol?({x, _}, %Map{width: width}) when x >= width, do: false
  def is_symbol?({_, y}, %Map{height: height}) when y >= height, do: false
  def is_symbol?({x, y}, %Map{content: content}) do
    val = content
    |> Enum.at(x)
    |> Enum.at(y)
    if val in @num_graphemes or val === ".", do: false, else: true
  end

  def is_real_part?(part, map) do
    neighbors(part)
    |> Enum.any?(fn neighbor -> is_symbol?(neighbor, map) end)
  end

  def part1(input) do
    map_content = input
    |> Stream.map(&String.trim/1)
    |> Stream.map(&String.graphemes/1)
    |> Enum.to_list()

    %Map{content: map_content, height: length(map_content), width: length(Enum.at(map_content, 0))}
    |> find_parts()
    |> Enum.map(fn %Part{value: value} -> value end)
    |> Enum.sum()
  end

  def find_gears(map, parts) do
    map.content
    |> Enum.with_index()
    |> Enum.map(fn {line, y} ->
      line
      |> Enum.with_index()
      |> find_gears_in_line(parts, y, map, [])
    end)
    |> List.flatten()
  end

  def find_gears_in_line([], _, _, _, gear_powers), do: gear_powers
  def find_gears_in_line([{"*", x} | rest], parts, y, map, gear_powers) do
    neighboring_part_values = parts
    |> Enum.reduce_while([], fn
      %Part{line: line}, acc when line < y-1 -> {:cont, acc}
      %Part{line: line}, acc when line > y+1 -> {:halt, acc}
      %Part{start_x: start_x}, acc when start_x > x+1 -> {:cont, acc}
      %Part{start_x: start_x, length: length}, acc when start_x+length < x -> {:cont, acc}
      %Part{value: value}, acc -> {:cont, [value | acc]}
    end)
    new_powers = if length(neighboring_part_values) === 2 do
      [Enum.product(neighboring_part_values) | gear_powers]
    else
      gear_powers
    end
    find_gears_in_line(rest, parts, y, map, new_powers)
  end
  def find_gears_in_line([_ | rest], parts, y, map, gear_powers), do: find_gears_in_line(rest, parts, y, map, gear_powers)

  def part2(input) do
    map_content = input
    |> Stream.map(&String.trim/1)
    |> Stream.map(&String.graphemes/1)
    |> Enum.to_list()

    map = %Map{content: map_content, height: length(map_content), width: length(Enum.at(map_content, 0))}

    parts = find_parts(map)

    find_gears(map, parts)
    |> Enum.sum()
  end
end

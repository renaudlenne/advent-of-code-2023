defmodule AdventOfCode.Day21 do
  def get_starting_point(map, steps) do
    map
    |> Enum.with_index()
    |> Enum.reduce_while(nil, fn
      {line, y}, nil ->
        case Enum.find_index(line, fn c -> c == "S" end) do
          nil -> {:cont, nil}
          x -> {:halt, {"S", {x, y}, steps}}
        end
    end)
  end

  def next_positions(map, {x, y}, steps) do
    [{x-1, y}, {x, y-1}, {x+1, y}, {x, y+1}]
    |> Enum.map(&({AdventOfCode.Utils.char_at(map, &1), &1, steps}))
    |> Enum.filter(fn {char, _, _} -> char != "#" end)
  end

  def walk(_, [{_, _, 0} | _] = last_positions), do: last_positions
  def walk(map, [{char, {x, y}, steps} | rest]) do
    new_steps = next_positions(map, {x, y}, steps-1)
    |> Enum.filter(fn v -> not(v in rest) end)
    walk(map, rest ++ new_steps)
  end

  def part1(input) do
    map = AdventOfCode.Utils.parse_map(input)
    starting_point = get_starting_point(map, 64)
    walk(map, [starting_point])
    |> MapSet.new(fn {_, coord, 0} -> coord end)
    |> MapSet.size()
  end

  def part2(input) do
  end
end

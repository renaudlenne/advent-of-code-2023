defmodule AdventOfCode.Day11 do
  def parse_universe(input) do
    input
    |> Enum.with_index()
    |> Enum.flat_map(fn {line, y} ->
      line
      |> String.trim()
      |> String.graphemes()
      |> Enum.with_index()
      |> Enum.reduce([], fn
        {"#", x}, acc -> [{x, y} | acc]
        _, acc -> acc
      end)
    end)
  end

  def grow(universe, expansion \\ 2) do
    {max_x, max_y} = universe
    |> Enum.reduce({0, 0}, fn
      {x, y}, {max_x, max_y} when x > max_x and y > max_y -> {x, y}
      {x, _}, {max_x, max_y} when x > max_x -> {x, max_y}
      {_, y}, {max_x, max_y} when y > max_y -> {max_x, y}
      _, max -> max
    end)

    empty_cols = 0..max_x
    |> Enum.reduce([], fn col, acc ->
      case Enum.all?(universe, fn {x, _} -> x !== col  end) do
        true -> [col | acc]
        _ -> acc
      end
    end)

    empty_lines = 0..max_y
    |> Enum.reduce([], fn line, acc ->
      case Enum.all?(universe, fn {_, y} -> y !== line  end) do
        true -> [line | acc]
        _ -> acc
      end
    end)

    universe
    |> Enum.map(fn {x, y} ->
      {x + Enum.count(empty_cols, fn col -> col < x end) * (expansion-1), y + Enum.count(empty_lines, fn line -> line < y end) * (expansion-1)}
    end)
  end

  def distance([{x1, y1}, {x2, y2}]), do: abs(x1-x2) + abs(y1-y2)

  def part1(input) do
    input
    |> parse_universe()
    |> grow()
    |> AdventOfCode.Utils.comb(2)
    |> Enum.map(&distance/1)
    |> Enum.sum()
  end

  def part2(input) do
    input
    |> parse_universe()
    |> grow(1000000)
    |> AdventOfCode.Utils.comb(2)
    |> Enum.map(&distance/1)
    |> Enum.sum()
  end
end

defmodule AdventOfCode.Day20 do
  use Agent

  defmodule Module do
    defstruct [:name, :type, :state, :destinations]
  end

  def parse_module(line) do
    [_, prefix, name, destinations] = Regex.run(~r"^([%&]?)([a-z]+) -> ([a-z ,]+)\n?$", line)
    module_type = case prefix do
      "%" -> :flip_flop
      "&" -> :conjunction
      _ -> :broadcast
    end
    %Module{
      name: name,
      type: module_type,
      state: case module_type do
        :flip_flop -> :off
        :conjunction -> %{}
        _ -> nil
      end,
      destinations:
        destinations
        |> String.split(",")
        |> Enum.map(&String.trim/1)
    }
  end

  def get_initial_state(modules) do
    modules
    |> Map.values()
    |> Enum.reduce(%{}, fn
      %Module{name: name, type: :conjunction, state: %{}}, acc ->
        state = Map.values(modules)
        |> Enum.filter(fn module -> name in module.destinations end)
        |> Enum.map(fn module -> {module.name, :low} end)
        |> Map.new()
        Map.put(acc, name, state)
      %Module{name: name, state: state}, acc -> Map.put(acc, name, state)
    end)
  end

  def switch(:off), do: :on
  def switch(:on), do: :off
  def freq_from_switch(:off), do: :high
  def freq_from_switch(:on), do: :low
  def update_counters(nb_low, nb_high, :high), do: {nb_low, nb_high+1}
  def update_counters(nb_low, nb_high, :low), do: {nb_low+1, nb_high}

  def pulse_from_conj_memory(memory) do
    case Map.values(memory) |> Enum.any?(fn mem -> mem != :high end) do
      true -> :high
      _ -> :low
    end
  end

  defmodule Pulse do
    defstruct [:sender, :freq, :dest]
  end

  def send_pulse(queue, modules, state, nb_low, nb_high)
  def send_pulse([], _, state, nb_low, nb_high), do: {state, nb_low, nb_high}

  def send_pulse([%Pulse{freq: freq, dest: nil, sender: sender} | queue], modules, state, nb_low, nb_high) do
#    IO.puts("#{sender} #{freq} -> unknown")
   {new_low, new_high} = update_counters(nb_low, nb_high, freq )
   send_pulse(queue, modules, state, new_low, new_high)
  end
  def send_pulse([%Pulse{freq: :high, dest: %Module{type: :flip_flop, name: name}, sender: sender} | queue], modules, state, nb_low, nb_high) do
#    IO.puts("#{sender} #{:high} -> #{name}")
    send_pulse(queue, modules, state, nb_low, nb_high+1)
  end
  def send_pulse([%Pulse{freq: :low, dest: %Module{type: :flip_flop, name: name, destinations: destinations}, sender: sender} | queue], modules, state, nb_low, nb_high) do
#    IO.puts("#{sender} #{:low} -> #{name}")
    switch_state = Map.get(state, name)
    new_state = Map.put(state, name, switch(switch_state))
    new_pulse_freq = freq_from_switch(switch_state)
    new_pulses = Enum.map(destinations, fn dest_name ->
      %Pulse{freq: new_pulse_freq, dest: Map.get(modules, dest_name), sender: name}
    end)
    send_pulse(queue ++ new_pulses, modules, new_state, nb_low+1, nb_high)
  end

  def send_pulse([%Pulse{freq: freq, dest: %Module{type: :broadcast, name: name, destinations: destinations}, sender: sender} | queue], modules, state, nb_low, nb_high) do
#    IO.puts("#{sender} #{freq} -> #{name}")
    {new_low, new_high} = update_counters(nb_low, nb_high, freq)
    new_pulses = Enum.map(destinations, fn dest_name ->
      %Pulse{freq: freq, dest: Map.get(modules, dest_name), sender: name}
    end)
    send_pulse(queue ++ new_pulses, modules, state, new_low, new_high)
  end

  def send_pulse([%Pulse{freq: freq, dest: %Module{type: :conjunction, name: name, destinations: destinations}, sender: sender} | queue], modules, state, nb_low, nb_high) do
#    IO.puts("#{sender} #{freq} -> #{name}")
    memory = Map.get(state, name) |> Map.put(sender, freq)
    new_pulse = pulse_from_conj_memory(memory)
    {new_low, new_high} = update_counters(nb_low, nb_high, freq)
    new_pulses = Enum.map(destinations, fn dest_name ->
      %Pulse{freq: new_pulse, dest: Map.get(modules, dest_name), sender: name}
    end)
    send_pulse(queue ++ new_pulses, modules, Map.put(state, name, memory), new_low, new_high)
  end

  def push_button(modules, state) do
#    IO.puts("---------------")
#    IO.puts("Pushing button")
    send_pulse([%Pulse{freq: :low, dest: Map.get(modules, "broadcaster"), sender: "button"}], modules, state, 0, 0)
  end

  def push_nb_times(modules, nb, agent_pid) do
    initial_state = get_initial_state(modules)
    0..(nb-1)
    |> Enum.reduce_while(initial_state, fn i, state ->
      case Agent.get(agent_pid, &(Map.get(&1, state))) do
        nil ->
          {new_state, nb_low, nb_high} = push_button(modules, state)
          Agent.update(agent_pid, &(Map.put(&1, state, {nb_low, nb_high, i})))
          {:cont, new_state}
        {stored_low, stored_high, iter} ->
          loop_size = i - iter
          remaining_rotations = nb - i
          nb_loops = div(remaining_rotations, loop_size)+1
          remaining_rotations_after_loop = rem(remaining_rotations, loop_size)
          {loop_low, loop_high} = Agent.get(agent_pid, fn map ->
            map
            |> Map.values()
            |> Enum.reduce({stored_low, stored_high}, fn
              {j_low, j_high, j}, {acc_low, acc_high} when j > iter -> {acc_low + j_low, acc_high + j_high}
              {j_low, j_high, j}, acc -> acc
            end)
          end)
#          IO.puts("{i=#{i}, iter=#{iter}, loop_size: #{loop_size}, remaining_rotations=#{remaining_rotations}, nb_loops=#{nb_loops}, remaining_rotations_after_loop=#{remaining_rotations_after_loop}, loop_low=#{loop_low}, loop_high=#{loop_high}, stored_low=#{stored_low}, stored_high=#{stored_high}}")
          loop_state = {state, loop_low*nb_loops, loop_high*nb_loops}
          if remaining_rotations_after_loop > 0 do
            {:halt, Enum.reduce(0..remaining_rotations_after_loop, loop_state, fn _, {new_state, acc_low, acc_high} ->
              {res_state, low, high} = push_button(modules, new_state)
              {res_state, acc_low+low, acc_high+high}
            end)}
          else
            {:halt, loop_state}
          end
      end
    end)
  end

  def score({_, nb_low, nb_high}, _), do: nb_low*nb_high
  def score(_, agent_pid) do
    {nb_low, nb_high} = Agent.get(agent_pid, fn map ->
      map
      |> Map.values()
      |> Enum.reduce({0, 0}, fn {nb_low, nb_high, _}, {acc_low, acc_high} ->
        {acc_low + nb_low, acc_high + nb_high}
      end)
    end)
    nb_low*nb_high
  end

  def part1(input) do
    {:ok, agent_pid} = Agent.start_link(fn -> %{} end)

    input
    |> Enum.map(&parse_module/1)
    |> Enum.reduce(%{}, fn module, map ->
      Map.put(map, module.name, module)
    end)
    |> push_nb_times(1000, agent_pid)
    |> score(agent_pid)
  end

  def part2(input) do
  end
end

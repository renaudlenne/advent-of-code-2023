defmodule AdventOfCode.Utils do
  def draw_map(map, symbol_for) do
    {min_x, max_x} =
      map
      |> Map.keys()
      |> Enum.map(fn {x, _} -> x end)
      |> Enum.min_max()

    {min_y, max_y} =
      map
      |> Map.keys()
      |> Enum.map(fn {_, y} -> y end)
      |> Enum.min_max()
    {min_x, max_x, min_y, max_y}

    for y <- min_y..max_y do
      for x <- min_x..max_x do
        case Map.get(map, {x, y}, 0) do
          0 -> IO.write(" ")
          v -> IO.write(symbol_for.(v))
        end
      end
      IO.write("\n")
    end

    map
  end

  def comb(_, 0), do: [[]]
  def comb([], _), do: []
  def comb([h|t], m) do
    (for l <- comb(t, m-1), do: [h|l]) ++ comb(t, m)
  end

  def parse_map(input) do
    input
    |> Enum.map(fn line ->
      line
      |> String.trim()
      |> String.graphemes()
    end)
  end

  def transpose(rows) do
    rows
    |> List.zip
    |> Enum.map(&Tuple.to_list/1)
  end

  def char_at(_, {-1, _}), do: "#"
  def char_at(_, {_, -1}), do: "#"
  def char_at(map, {x, y}) do
    max_y = length(map)
    if (y == max_y) do
      "#"
    else
      max_x = length(hd(map))
      if (x == max_x) do
        "#"
      else
        Enum.at(map, y) |> Enum.at(x)
      end
    end
  end

end
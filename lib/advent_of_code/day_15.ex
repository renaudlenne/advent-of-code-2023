defmodule AdventOfCode.Day15 do
  def hash(str) do
    str
    |> to_charlist()
    |> Enum.reduce(0, fn val, acc -> rem((acc + val) * 17, 256) end)
  end

  def part1(input) do
    input
    |> Stream.flat_map(fn line ->
      line
      |> String.trim()
      |> String.split(",")
      |> Enum.map(&hash/1)
    end)
    |> Enum.sum()
  end

  defmodule Lens do
    defstruct [:label, :focal]
  end

  def apply_step(boxes, box_nb, label, "=", focal) do
    current_box = Map.get(boxes, box_nb, [])
    lens = %Lens{label: label, focal: String.to_integer(focal)}
    box = case Enum.find_index(current_box, fn
      %Lens{label: ^label} -> true
      _ -> false
    end) do
      nil -> [lens | current_box]
      idx -> List.replace_at(current_box, idx, lens)
    end
    Map.put(boxes, box_nb, box)
  end
  def apply_step(boxes, box_nb, label, "-", "") do
    current_box = Map.get(boxes, box_nb, [])
    box = case Enum.find_index(current_box, fn
      %Lens{label: ^label} -> true
      _ -> false
    end) do
      nil -> current_box
      idx -> List.delete_at(current_box, idx)
    end
    Map.put(boxes, box_nb, box)
  end

  def part2(input) do
    input
    |> Enum.reduce(%{}, fn line, boxes ->
      line
      |> String.trim()
      |> String.split(",")
      |> Enum.reduce(boxes, fn step, line_boxes ->
        [_, label, operator, focal] = Regex.run(~r"^([a-z]+)([=\-])([0-9]*)$", step)
        apply_step(line_boxes, hash(label), label, operator, focal)
      end)
    end)
    |> Map.to_list()
    |> Enum.map(fn {box_nb, lenses} ->
      lenses
      |> Enum.reverse()
      |> Enum.with_index()
      |> Enum.map(fn {%Lens{focal: focal}, lens_slot} ->
        (box_nb+1) * (lens_slot+1) * focal
      end)
      |> Enum.sum()
    end)
    |> Enum.sum()
  end
end

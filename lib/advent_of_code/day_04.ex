defmodule AdventOfCode.Day04 do

  def score(nb_matches, previous_score \\ 0)
  def score(0, prev), do: prev
  def score(curr, 0), do: score(curr-1, 1)
  def score(curr, prev), do: score(curr-1, prev*2)

  def parse_line(line), do: Regex.run(~r"^Card +(\d+):([\d ]+)\|([\d ]+)\n?$", line)

  def count_wins(scratched_raw, winning_raw) do
    scratched_raw
    |> String.trim()
    |> String.split(~r" +")
    |> Enum.map(&String.trim/1)
    |> Enum.count(fn scratched_number -> String.contains?(winning_raw, " #{scratched_number} ") end)
  end

  def part1(input) do
    input
    |> Stream.map(fn line ->
      [_, _, winning_raw, scratched_raw] = parse_line(line)

      count_wins(scratched_raw, winning_raw)
      |> score()
    end)
    |> Enum.sum()
  end

  def part2(input) do
    input
    |> Stream.with_index()
    |> Enum.reduce({%{}, 0}, fn {line, idx}, {won_cards, _} ->
      [_, id_raw, winning_raw, scratched_raw] = parse_line(line)
      id = String.to_integer(id_raw)

      case count_wins(scratched_raw, winning_raw) do
        0 -> {won_cards, idx+1}
        nb_wins ->
          id+1..id+nb_wins
          |> Enum.reduce(won_cards, fn new_id, acc ->
            Map.put(acc, new_id, Map.get(acc, new_id, 0) + 1 + Map.get(acc, id, 0))
          end)
          |> then(fn new_won_cards -> {new_won_cards, idx+1} end)
      end
    end)
    |> then(fn {won_cards, nb_initial_cards} ->
      nb_initial_cards + (Map.values(won_cards) |> Enum.sum())
    end)
  end
end

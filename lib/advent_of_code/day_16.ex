defmodule AdventOfCode.Day16 do
  use Agent


  def if_new_node({x, y}, dir, agent_pid, callback, else_clbk) do
    case Agent.get(agent_pid, &({x, y, dir} in &1)) do
      false ->
        Agent.update(agent_pid, &([{x, y, dir} | &1]))
        callback.()
      true -> else_clbk.()
    end
  end

  def go_down(map, x, y), do: {AdventOfCode.Utils.char_at(map, {x, y+1}), {x, y+1}, :down}
  def go_right(map, x, y), do: {AdventOfCode.Utils.char_at(map, {x+1, y}), {x+1, y}, :right}
  def go_left(map, x, y), do: {AdventOfCode.Utils.char_at(map, {x-1, y}), {x-1, y}, :left}
  def go_up(map, x, y), do: {AdventOfCode.Utils.char_at(map, {x, y-1}), {x, y-1}, :up}

  def next_beams(map, ".", {x, y}, :right), do: [go_right(map, x, y)]
  def next_beams(map, ".", {x, y}, :up), do: [go_up(map, x, y)]
  def next_beams(map, ".", {x, y}, :left), do: [go_left(map, x, y)]
  def next_beams(map, ".", {x, y}, :down), do: [go_down(map, x, y)]

  def next_beams(map, "|", {x, y}, :up), do: [go_up(map, x, y)]
  def next_beams(map, "|", {x, y}, :down), do: [go_down(map, x, y)]
  def next_beams(map, "-", {x, y}, :right), do: [go_right(map, x, y)]
  def next_beams(map, "-", {x, y}, :left), do: [go_left(map, x, y)]

  def next_beams(map, "/", {x, y}, :right), do: [go_up(map, x, y)]
  def next_beams(map, "/", {x, y}, :up), do: [go_right(map, x, y)]
  def next_beams(map, "/", {x, y}, :left), do: [go_down(map, x, y)]
  def next_beams(map, "/", {x, y}, :down), do: [go_left(map, x, y)]

  def next_beams(map, "\\", {x, y}, :right), do: [go_down(map, x, y)]
  def next_beams(map, "\\", {x, y}, :up), do: [go_left(map, x, y)]
  def next_beams(map, "\\", {x, y}, :left), do: [go_up(map, x, y)]
  def next_beams(map, "\\", {x, y}, :down), do: [go_right(map, x, y)]

  def next_beams(map, "|", {x, y}, :right), do: [go_up(map, x, y), go_down(map, x, y)]
  def next_beams(map, "|", {x, y}, :left), do: [go_up(map, x, y), go_down(map, x, y)]

  def next_beams(map, "-", {x, y}, :up), do: [go_left(map, x, y), go_right(map, x, y)]
  def next_beams(map, "-", {x, y}, :down), do: [go_left(map, x, y), go_right(map, x, y)]

  def follow_beam(_, [], energized, _), do: MapSet.new(energized) |> MapSet.size()
  def follow_beam(map, [{"#", _, _} | rest], energized, agent_pid), do: follow_beam(map, rest, energized, agent_pid)
  def follow_beam(map, [{char, coord, direction} | rest], energized, agent_pid) do
    if_new_node(coord, direction, agent_pid, fn ->
      follow_beam(map, next_beams(map, char, coord, direction) ++ rest, [coord | energized], agent_pid)
    end, fn -> follow_beam(map, rest, energized, agent_pid) end)
  end

  def part1(input) do
    {:ok, agent_pid} = Agent.start_link(fn -> [] end)

    map = AdventOfCode.Utils.parse_map(input)
    initial_char = AdventOfCode.Utils.char_at(map, {0, 0})
    follow_beam(map, [{initial_char, {0, 0}, :right}], [], agent_pid)
  end

  def part2(input) do
    map = AdventOfCode.Utils.parse_map(input)
    max_x = length(map)-1
    max_y = length(hd(map))-1
    Enum.flat_map(0..max_x, fn x ->
      [
        {AdventOfCode.Utils.char_at(map, {x, 0}), {x, 0}, :down},
        {AdventOfCode.Utils.char_at(map, {x, max_y}), {x, max_y}, :up},
      ]
    end) ++ Enum.flat_map(0..max_y, fn y ->
      [
        {AdventOfCode.Utils.char_at(map, {max_x, y}), {max_x, y}, :left},
        {AdventOfCode.Utils.char_at(map, {0, y}), {0, y}, :right},
      ]
    end)
    |> Task.async_stream(fn starting ->
      {:ok, agent_pid} = Agent.start_link(fn -> [] end)
      follow_beam(map, [starting], [], agent_pid)
    end)
    |> Enum.reduce(0, fn
      {:ok, num}, acc when num > acc -> num
      {:ok, _}, acc -> acc
    end)
  end
end

defmodule AdventOfCode.Day09 do

  def find_next(line) do
    next_line = get_diffs(line, [])
    case Map.keys(Enum.frequencies(next_line)) do
      [v] -> List.last(line) + v
      _ -> List.last(line) + find_next(next_line)
    end
  end

  def get_diffs([a, b], acc), do: Enum.reverse([b-a | acc])
  def get_diffs([a, b | rest], acc), do: get_diffs([b | rest], [b-a | acc])

  def part1(input) do
    input
    |> Stream.map(&String.split/1)
    |> Stream.map(fn line ->
      line
      |> Enum.map(&String.to_integer/1)
      |> find_next()
    end)
    |> Enum.sum()
  end

  def part2(input) do
    input
    |> Stream.map(&String.split/1)
    |> Stream.map(fn line ->
      line
      |> Enum.map(&String.to_integer/1)
      |> Enum.reverse()
      |> find_next()
    end)
    |> Enum.sum()
  end
end

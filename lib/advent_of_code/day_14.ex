defmodule AdventOfCode.Day14 do
  use Agent

  def move_boulders_left(line, prev \\ [])
  def move_boulders_left([], prev), do: prev
  def move_boulders_left(["O" | next], ["." | prev]), do: move_boulders_left(["O", "." | next], prev)
  def move_boulders_left(["O" | next], prev), do: move_boulders_left(next, ["O" | prev])
  def move_boulders_left([curr | next], prev), do: move_boulders_left(next, [curr | prev])

  def score_line(line) do
    line
    |> Enum.with_index()
    |> Enum.map(fn
      {"O", idx} -> idx+1
      _ -> 0
    end)
    |> Enum.sum()
  end

  def part1(input) do
    input
    |> AdventOfCode.Utils.parse_map()
    |> AdventOfCode.Utils.transpose()
    |> Enum.map(&move_boulders_left/1)
    |> Enum.map(&score_line/1)
    |> Enum.sum()
  end

  def cycle_rotation(map) do
    1..4
    |> Enum.reduce(map, fn _, acc ->
      acc
      |> AdventOfCode.Utils.transpose()
      |> Enum.map(&move_boulders_left/1)
    end)
  end

  def do_nb_cycles(map, nb, agent_pid) do
    1..nb
    |> Enum.reduce_while(map, fn i, current_rotation ->
      case Agent.get(agent_pid, &(Map.get(&1, current_rotation))) do
        nil ->
          Agent.update(agent_pid, &(Map.put(&1, current_rotation, i)))
          {:cont, cycle_rotation(current_rotation)}
        v ->
          loop_size = i - v
          remaining_rotations = nb - i
          remaining_rotations_after_loop = rem(remaining_rotations, loop_size)
          {:halt, Enum.reduce(0..remaining_rotations_after_loop, current_rotation, fn _, acc -> cycle_rotation(acc) end)}
      end
    end)
  end

  def part2(input) do
    {:ok, agent_pid} = Agent.start_link(fn -> %{} end)

    input
    |> AdventOfCode.Utils.parse_map()
    |> do_nb_cycles(1000000000, agent_pid)
    |> AdventOfCode.Utils.transpose()
    |> Enum.map(fn line -> Enum.reverse(line) |> score_line() end)
    |> Enum.sum()
  end
end

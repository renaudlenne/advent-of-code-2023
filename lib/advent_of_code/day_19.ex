defmodule AdventOfCode.Day19 do
  defmodule Rule do
    defstruct [:value, :operator, :check, :destination]
  end
  defmodule DefaultRule do
    defstruct [:destination]
  end
  defmodule Part do
    defstruct [:x, :m, :a, :s]
  end

  def parse_rule(rule) do
    case Regex.run(~r"^(([xmas])([<>])(\d+):)?([a-zAR]+)$", rule) do
      [_, "", "", "", "", dest] -> %DefaultRule{destination: dest}
      [_, _, value, operator, check, dest] -> %Rule{value: value, operator: operator, check: String.to_integer(check), destination: dest}
    end
  end

  def parse_workflow(line) do
    [_, name, data] = Regex.run(~r"^([a-z]+)\{(.+)\}\n?$", line)
    {name, String.split(data, ",") |> Enum.map(&parse_rule/1)}
  end

  def parse_part(line) do
    [_, x, m, a, s] = Regex.run(~r"^\{x=(\d+),m=(\d+),a=(\d+),s=(\d+)\}", line)
    %Part{x: String.to_integer(x), m: String.to_integer(m), a: String.to_integer(a), s: String.to_integer(s)}
  end

  def parse_input(input, with_parts \\ true) do
    input
    |> Stream.chunk_while(%{}, fn
      "\n", acc ->
        if with_parts, do: {:cont, {acc, []}}, else: {:halt, acc}
      line, {workflows, parts} -> {:cont, {workflows, [parse_part(line) | parts]}}
      line, workflows ->
        {name, rules} = parse_workflow(line)
        {:cont, Map.put(workflows, name, rules)}
    end, fn res -> {:cont, res, nil} end)
    |> Enum.to_list()
    |> hd()
  end

  def check_rule(rule, part)
  def check_rule(%DefaultRule{destination: dest}, _), do: {:ok, dest}
  def check_rule(%Rule{value: "x", operator: "<", check: check, destination: dest}, %Part{x: x}) when x < check, do: {:ok, dest}
  def check_rule(%Rule{value: "m", operator: "<", check: check, destination: dest}, %Part{m: m}) when m < check, do: {:ok, dest}
  def check_rule(%Rule{value: "a", operator: "<", check: check, destination: dest}, %Part{a: a}) when a < check, do: {:ok, dest}
  def check_rule(%Rule{value: "s", operator: "<", check: check, destination: dest}, %Part{s: s}) when s < check, do: {:ok, dest}
  def check_rule(%Rule{value: "x", operator: ">", check: check, destination: dest}, %Part{x: x}) when x > check, do: {:ok, dest}
  def check_rule(%Rule{value: "m", operator: ">", check: check, destination: dest}, %Part{m: m}) when m > check, do: {:ok, dest}
  def check_rule(%Rule{value: "a", operator: ">", check: check, destination: dest}, %Part{a: a}) when a > check, do: {:ok, dest}
  def check_rule(%Rule{value: "s", operator: ">", check: check, destination: dest}, %Part{s: s}) when s > check, do: {:ok, dest}
  def check_rule(_, _), do: {:nop}

  def run_workflow(part, rules, workflows) do
    Enum.reduce_while(rules, nil, fn rule, _ ->
      case check_rule(rule, part) do
        {:nop} -> {:cont, nil}
        {:ok, "A"} -> {:halt, true}
        {:ok, "R"} -> {:halt, false}
        {:ok, dest} -> {:halt, run_workflow(part, Map.get(workflows, dest), workflows)}
      end
    end)
  end

  def score_part(%Part{x: x, m: m, a: a, s: s}), do: x+m+a+s

  def part1(input) do
    {workflows, parts} = parse_input(input)
    initial_workflow = Map.get(workflows, "in")
    parts
    |> Enum.filter(&(run_workflow(&1, initial_workflow, workflows)))
    |> Enum.map(&score_part/1)
    |> Enum.sum()
  end

  defmodule Range do
    defstruct [:min, :max]
  end

  def split_range(rule, range)
  def split_range(%DefaultRule{destination: dest}, _), do: {:ok, dest}

  def split_range(%Rule{value: "x", operator: "<", check: check, destination: dest}, %Part{x: %Range{max: maxx}}=part) when maxx < check, do: {:ok, dest}
  def split_range(%Rule{value: "x", operator: "<", check: check, destination: dest}, %Part{x: %Range{min: minx}=rangex}=part) when minx < check, do: {:ok, [{%Part{part | x: %Range{min: minx, max: check-1}}, dest}, %Part{part | x: %Range{rangex | min: check}}]}
  def split_range(%Rule{value: "m", operator: "<", check: check, destination: dest}, %Part{m: %Range{max: maxm}}=part) when maxm < check, do: {:ok, dest}
  def split_range(%Rule{value: "m", operator: "<", check: check, destination: dest}, %Part{m: %Range{min: minm}=rangem}=part) when minm < check, do: {:ok, [{%Part{part | m: %Range{min: minm, max: check-1}}, dest}, %Part{part | m: %Range{rangem | min: check}}]}
  def split_range(%Rule{value: "a", operator: "<", check: check, destination: dest}, %Part{a: %Range{max: maxa}}=part) when maxa < check, do: {:ok, dest}
  def split_range(%Rule{value: "a", operator: "<", check: check, destination: dest}, %Part{a: %Range{min: mina}=rangea}=part) when mina < check, do: {:ok, [{%Part{part | a: %Range{min: mina, max: check-1}}, dest}, %Part{part | a: %Range{rangea | min: check}}]}
  def split_range(%Rule{value: "s", operator: "<", check: check, destination: dest}, %Part{s: %Range{max: maxs}}=part) when maxs < check, do: {:ok, dest}
  def split_range(%Rule{value: "s", operator: "<", check: check, destination: dest}, %Part{s: %Range{min: mins}=ranges}=part) when mins < check, do: {:ok, [{%Part{part | s: %Range{min: mins, max: check-1}}, dest}, %Part{part | s: %Range{ranges | min: check}}]}

  def split_range(%Rule{value: "x", operator: ">", check: check, destination: dest}, %Part{x: %Range{min: minx}}=part) when minx > check, do: {:ok, dest}
  def split_range(%Rule{value: "x", operator: ">", check: check, destination: dest}, %Part{x: %Range{max: maxx}=rangex}=part) when maxx > check, do: {:ok, [{%Part{part | x: %Range{max: maxx, min: check+1}}, dest}, %Part{part | x: %Range{rangex | max: check}}]}
  def split_range(%Rule{value: "m", operator: ">", check: check, destination: dest}, %Part{m: %Range{min: minm}}=part) when minm > check, do: {:ok, dest}
  def split_range(%Rule{value: "m", operator: ">", check: check, destination: dest}, %Part{m: %Range{max: maxm}=rangem}=part) when maxm > check, do: {:ok, [{%Part{part | m: %Range{max: maxm, min: check+1}}, dest}, %Part{part | m: %Range{rangem | max: check}}]}
  def split_range(%Rule{value: "a", operator: ">", check: check, destination: dest}, %Part{a: %Range{min: mina}}=part) when mina > check, do: {:ok, dest}
  def split_range(%Rule{value: "a", operator: ">", check: check, destination: dest}, %Part{a: %Range{max: maxa}=rangea}=part) when maxa > check, do: {:ok, [{%Part{part | a: %Range{max: maxa, min: check+1}}, dest}, %Part{part | a: %Range{rangea | max: check}}]}
  def split_range(%Rule{value: "s", operator: ">", check: check, destination: dest}, %Part{s: %Range{min: mins}}=part) when mins > check, do: {:ok, dest}
  def split_range(%Rule{value: "s", operator: ">", check: check, destination: dest}, %Part{s: %Range{max: maxs}=ranges}=part) when maxs > check, do: {:ok, [{%Part{part | s: %Range{max: maxs, min: check+1}}, dest}, %Part{part | s: %Range{ranges | max: check}}]}

  def split_range(_, _), do: {:nop}

  def get_accepted_range(part_range, rules, workflows) do
    Enum.reduce_while(rules, {part_range, []}, fn rule, {range, acc} ->
      case split_range(rule, range) do
        {:nop} -> {:cont, {range, acc}}
        {:ok, "A"} -> {:halt, [range | acc]}
        {:ok, "R"} -> {:halt, acc}
        {:ok, [{range1, "A"}, range2]} -> {:cont, {range2, [range1 | acc]}}
        {:ok, [{_, "R"}, range2]} -> {:cont, {range2, acc}}
        {:ok, [{range1, dest}, range2]} -> {:cont, {range2, get_accepted_range(range1, Map.get(workflows, dest), workflows) ++ acc}}
        {:ok, dest} -> {:halt, get_accepted_range(range, Map.get(workflows, dest), workflows) ++ acc}
      end
    end)
  end

  def count_parts(part_ranges) do
    part_ranges
    |> Enum.reduce(0, fn %Part{x: %Range{min: minx, max: maxx}, m: %Range{min: minm, max: maxm}, a: %Range{min: mina, max: maxa}, s: %Range{min: mins, max: maxs}}, acc ->
      acc + (maxx+1-minx)*(maxm+1-minm)*(maxa+1-mina)*(maxs+1-mins)
    end)
  end

  def part2(input) do
    workflows = parse_input(input, false)
    initial_workflow = Map.get(workflows, "in")
    get_accepted_range(%Part{x: %Range{min: 1, max: 4000}, m: %Range{min: 1, max: 4000}, a: %Range{min: 1, max: 4000}, s: %Range{min: 1, max: 4000}}, initial_workflow, workflows)
    |> count_parts()
  end
end

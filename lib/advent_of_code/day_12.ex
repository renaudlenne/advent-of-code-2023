defmodule AdventOfCode.Day12 do
  use Agent

  def parse_line(line) do
    [_, record, damaged_groups] =  Regex.run(~r"^([\.\?#]+) ([\d,]+)\n?$", line)
    {record |> String.graphemes(), damaged_groups |> String.split(",") |> Enum.map(&String.to_integer/1)}
  end

  def count_possible_arrangement({record, damaged_groups}) do
    {:ok, agent_pid} = Agent.start_link(fn -> %{} end)
    memo_count_possible_arrangement(record, damaged_groups, 0, agent_pid)
  end

  def memo_count_possible_arrangement(record, groups, current_group, agent_pid) do
    case Agent.get(agent_pid, &(Map.get(&1, {record, groups, current_group}))) do
      nil ->
        v = count_possible_arrangement(record, groups, current_group, agent_pid)
        Agent.update(agent_pid, &(Map.put(&1, {record, groups, current_group}, v)))
        v
      v -> v
    end
  end

  def count_possible_arrangement([], [], _, _), do: 1
  def count_possible_arrangement([], [group], group, _), do: 1
  def count_possible_arrangement([], _, _, _), do: 0
  def count_possible_arrangement(["?" | rest ], groups, group_size, agent_pid) do
    memo_count_possible_arrangement(["#" | rest], groups, group_size, agent_pid) + memo_count_possible_arrangement(["." | rest], groups, group_size, agent_pid)
  end
  def count_possible_arrangement(["#" | _ ], [], _, _), do: 0
  def count_possible_arrangement(["#" | _ ], [group_size | _], group_size, _), do: 0
  def count_possible_arrangement(["#" | rest ], groups, group_size, agent_pid), do: memo_count_possible_arrangement(rest, groups, group_size+1, agent_pid)

  def count_possible_arrangement(["." | rest ], groups, 0, agent_pid), do: memo_count_possible_arrangement(rest, groups, 0, agent_pid)
  def count_possible_arrangement(["." | _ ], [group_size | _], current_group, _) when current_group < group_size, do: 0
  def count_possible_arrangement(["." | rest ], [group_size | next_groups], group_size, agent_pid), do: memo_count_possible_arrangement(rest, next_groups, 0, agent_pid)

  def part1(input) do
    input
    |> Stream.map(&parse_line/1)
    |> Stream.map(&count_possible_arrangement/1)
    |> Enum.sum()
  end

  def unfold_line({record, damaged_groups}) do
    1..4
    |> Enum.reduce({record, damaged_groups}, fn
      _, {acc_r, acc_g} -> { acc_r ++ ["?"] ++ record, acc_g ++ damaged_groups }
    end)
  end

  def part2(input) do
    input
    |> Stream.map(&parse_line/1)
    |> Stream.map(&unfold_line/1)
    |> Task.async_stream(&count_possible_arrangement/1, ordered: false, timeout: :infinity)
    |> Enum.reduce(0, fn {:ok, num}, acc -> num + acc end)
  end
end

defmodule AdventOfCode.Day05 do
  defmodule Almanac do
    defstruct [:seeds, :maps]
  end
  defmodule ConversionMap do
    defstruct [:source, :destination, :ranges]
  end
  defmodule Range do
    defstruct [:start_source, :start_destination, :range_length]
  end

  def parse_range(line) do
    [_, start_destination, start_source, range_length] = Regex.run(~r"^(\d+) +(\d+) +(\d+)\n?$", line)
    %Range{start_destination: String.to_integer(start_destination), start_source: String.to_integer(start_source), range_length: String.to_integer(range_length)}
  end

  def parse_almanac(input) do
    input
    |> Stream.chunk_while(nil, fn
      line, nil ->
        [_, seeds, _] = Regex.run(~r"^seeds\:(( +\d+)+)\n?$", line)
        {:cont, %Almanac{seeds: String.split(seeds) |> Enum.map(&String.to_integer/1), maps: %{}}}
      "\n", {almanac, current_map} ->{:cont, %Almanac{almanac | maps: Map.put(almanac.maps, current_map.source, current_map)}}
      "\n", acc -> {:cont, acc}
      line, {almanac, current_map} -> {:cont, {almanac, %ConversionMap{current_map | ranges: Enum.sort_by([parse_range(line) | current_map.ranges], fn %Range{start_source: start_source} -> start_source end)}}}
      line, almanac ->
        [_, source, destination] = Regex.run(~r"^([a-z]+)-to-([a-z]+) map:\n?$", line)
        {:cont, {almanac, %ConversionMap{source: source, destination: destination, ranges: []}}}
    end, fn {almanac, current_map} -> {:cont, %Almanac{almanac | maps: Map.put(almanac.maps, current_map.source, current_map)}, nil} end)
    |> Enum.to_list()
    |> hd()
  end

  def get_seed_location(seed, maps), do: get_conversion(seed, maps, "seed")

  def get_range_conversion(source, []), do: source
  def get_range_conversion(source, [%Range{start_source: start_source, start_destination: start_destination, range_length: range_length} | _]) when source >= start_source and source < start_source+range_length, do: source-start_source+start_destination
  def get_range_conversion(source, [_ | ranges]), do: get_range_conversion(source, ranges)

  def get_conversion(source, _, "location"), do: source
  def get_conversion(source, maps, source_type) do
    map = Map.get(maps, source_type)
    get_conversion(get_range_conversion(source, map.ranges), maps, map.destination)
  end

  def part1(input) do
    almanac = parse_almanac(input)

    almanac.seeds
    |> Enum.map(fn seed -> get_seed_location(seed, almanac.maps) end)
    |> Enum.min()
  end

  def get_min_seed_location(range, maps) do
    get_min_conversion(range, maps, "seed")
  end

  def get_min_conversion([source_start, _], _, "location"), do: source_start
  def get_min_conversion(source, maps, source_type) do
    map = Map.get(maps, source_type)
    map.ranges
    |> Enum.reduce_while({source, []}, fn
      %Range{start_source: range_start}, {[src_start, src_length] = src, acc} when range_start > src_start+src_length-1  ->
        #IO.puts("#{inspect range} fully after #{inspect(src, charlists: :as_lists)}")
        {:halt, [src | acc]}
      %Range{start_source: range_start, range_length:  range_length}, {[src_start, _] = src, acc} when src_start > range_start+range_length-1  ->
        #IO.puts("#{inspect range} fully before #{inspect(src, charlists: :as_lists)}")
        {:cont, {src, acc}}
      %Range{start_source: range_start, range_length:  range_length, start_destination: dst_start}, {[src_start, src_length], acc} when range_start <= src_start and (range_start + range_length) > (src_start + src_length-1)   ->
        #IO.puts("#{inspect range} fully contains #{inspect(src, charlists: :as_lists)}")
        {:halt, [[src_start-range_start+dst_start, src_length] |acc]}
      %Range{start_source: range_start, range_length:  range_length, start_destination: dst_start}, {[src_start, src_length], acc} when range_start >= src_start and (range_start + range_length) < (src_start + src_length-1)   ->
        #IO.puts("#{inspect range} fully inside #{inspect(src, charlists: :as_lists)}")
        {:cont, {[src_start+range_length+range_start-src_start, src_length-range_length-(range_start-src_start)], [[src_start, range_start-src_start], [dst_start, range_length] |acc]}}
      %Range{start_source: range_start, start_destination: dst_start}, {[src_start, src_length], acc} when range_start > src_start ->
        #IO.puts("#{inspect range} at the end of #{inspect(src, charlists: :as_lists)}")
        {:halt, [[src_start, range_start-src_start], [dst_start, src_length-(range_start-src_start)] |acc]}
      %Range{start_source: range_start, range_length:  range_length, start_destination: dst_start}, {[src_start, src_length], acc} ->
        #IO.puts("#{inspect range} at the start of #{inspect(src, charlists: :as_lists)}")
        {:cont, {[src_start+range_length-(src_start-range_start), src_length-(range_length-(src_start-range_start))], [[dst_start+(src_start-range_start), range_length-(src_start-range_start)] |acc]}}
    end)
    |> Enum.map(fn new_source -> get_min_conversion(new_source, maps, map.destination) end)
    |> Enum.min()
  end

  def part2(input) do
    almanac = parse_almanac(input)

    almanac.seeds
    |> Enum.chunk_every(2)
    |> Enum.map(fn range -> get_min_seed_location(range, almanac.maps) end)
    |> Enum.min()
  end
end

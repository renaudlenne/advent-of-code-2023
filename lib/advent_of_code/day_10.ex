defmodule AdventOfCode.Day10 do

  def find_start(map) do
    map
    |> Enum.with_index()
    |> Enum.reduce_while(nil, fn {line, idx}, _ ->
      case Enum.find_index(line, &(&1 === "S")) do
        nil -> {:cont, nil}
        v -> {:halt, {v, idx}}
      end
    end)
  end

  def elem_at(_, {-1, _}), do: nil
  def elem_at(_, {_, -1}), do: nil
  def elem_at(map, {_, y}) when y >= length(map), do: nil
  def elem_at([l1 | _], {x, _}) when x >= length(l1), do: nil
  def elem_at(map, {x, y}), do: Enum.at(map, y) |> Enum.at(x)

  def go_to(dir, map, pos, size), do: loop_size(map, pos, elem_at(map, pos), dir, size+1)

  def go(:n, map, {x, y}, size), do: go_to(:n, map, {x, y-1}, size)
  def go(:s, map, {x, y}, size), do: go_to(:s, map, {x, y+1}, size)
  def go(:e, map, {x, y}, size), do: go_to(:e, map, {x+1, y}, size)
  def go(:w, map, {x, y}, size), do: go_to(:w, map, {x-1, y}, size)

  def loop_size(_, _, nil, _, _), do: nil
  def loop_size(_, _, ".", _, _), do: nil
  def loop_size(_, _, "S", _, size), do: size
  def loop_size(map, pos, "|", :s, size), do: go(:s, map, pos, size)
  def loop_size(map, pos, "|", :n, size), do: go(:n, map, pos, size)
  def loop_size(_, _, "|", _, _), do: nil
  def loop_size(map, pos, "-", :e, size), do: go(:e, map, pos, size)
  def loop_size(map, pos, "-", :w, size), do: go(:w, map, pos, size)
  def loop_size(_, _, "-", _, _), do: nil
  def loop_size(map, pos, "L", :w, size), do: go(:n, map, pos, size)
  def loop_size(map, pos, "L", :s, size), do: go(:e, map, pos, size)
  def loop_size(_, _, "L", _, _), do: nil
  def loop_size(map, pos, "J", :s, size), do: go(:w, map, pos, size)
  def loop_size(map, pos, "J", :e, size), do: go(:n, map, pos, size)
  def loop_size(_, _, "J", _, _), do: nil
  def loop_size(map, pos, "7", :n, size), do: go(:w, map, pos, size)
  def loop_size(map, pos, "7", :e, size), do: go(:s, map, pos, size)
  def loop_size(_, _, "7", _, _), do: nil
  def loop_size(map, pos, "F", :n, size), do: go(:e, map, pos, size)
  def loop_size(map, pos, "F", :w, size), do: go(:s, map, pos, size)
  def loop_size(_, _, "F", _, _), do: nil

  def farthest_in_loop({x, y}, map) do
    [{{x-1,y}, :w}, {{x, y-1}, :n}, {{x+1, y}, :e}, {{x, y+1}, :s}]
    |> Enum.reduce_while(nil, fn {pos, dir}, _ ->
      case loop_size(map, pos, elem_at(map, pos), dir, 1) do
        nil -> {:cont, nil}
        s -> {:halt, div(s, 2)}
      end
    end)
  end

  def part1(input) do
    map = input
    |> Enum.map(fn line ->
      line
      |> String.trim()
      |> String.graphemes()
    end)

    find_start(map)
    |> farthest_in_loop(map)
  end

  def part2(_args) do
  end
end

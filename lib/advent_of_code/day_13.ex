defmodule AdventOfCode.Day13 do
  def find_line_reflexion(map, avoid_line) do
    1..length(map)-1
    |> Enum.reduce_while(0, fn i, _ ->
      if i == avoid_line do
        {:cont, 0}
      else
        {before_mirror, after_mirror} = Enum.split(map, i)
        case is_reflexion?(Enum.reverse(before_mirror), after_mirror) do
          true -> {:halt, i}
          false -> {:cont, 0}
        end
      end
    end)
  end

  def is_reflexion?([], _), do: true
  def is_reflexion?(_, []), do: true
  def is_reflexion?([a | rest1], [a | rest2]), do: is_reflexion?(rest1, rest2)
  def is_reflexion?(_, _), do: false

  def find_reflexions(map, avoid_line \\ 0, avoid_col \\ 0), do: %{line: find_line_reflexion(map, avoid_line), col: find_line_reflexion(AdventOfCode.Utils.transpose(map), avoid_col)}

  def parse_maps(input) do
    Stream.chunk_while(input, [], fn
      "\n", acc -> {:cont, Enum.reverse(acc), []}
      line, acc -> {:cont, [line | acc]}
    end, fn acc -> {:cont, Enum.reverse(acc), nil} end)
end

  def part1(input) do
    input
    |> parse_maps()
    |> Enum.map(fn raw_map ->
      AdventOfCode.Utils.parse_map(raw_map)
      |> find_reflexions()
      |> then(fn %{line: line, col: col} -> 100*line + col end)
    end)
    |> Enum.sum()
  end

  def all_permutations(map) do
    map
    |> Enum.with_index()
    |> Enum.flat_map(fn {line, y} ->
      line
      |> Enum.with_index()
      |> Enum.map(fn
        {"#", x} -> List.replace_at(map, y, List.replace_at(line, x, "."))
        {".", x} -> List.replace_at(map, y, List.replace_at(line, x, "#"))
      end)
    end)
  end

  def part2(input) do
    input
    |> parse_maps()
    |> Enum.map(fn raw_map ->
      map = AdventOfCode.Utils.parse_map(raw_map)
      %{line: initial_line, col: initial_col} = find_reflexions(map)
      all_permutations(map)
      |> Enum.map(fn permutation -> find_reflexions(permutation, initial_line, initial_col) end)
      |> Enum.reduce_while(nil, fn
        %{line: 0, col: 0}, _ -> {:cont, nil}
        %{line: line, col: col}, _ -> {:halt, %{line: line, col: col}}
      end)
      |> then(fn %{line: line, col: col} -> 100*line + col end)
    end)
    |> Enum.sum()
  end
end

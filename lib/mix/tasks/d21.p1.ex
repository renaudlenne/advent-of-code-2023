defmodule Mix.Tasks.D21.P1 do
  use Mix.Task

  import AdventOfCode.Day21
  defp config, do: Application.get_env(:advent_of_code, AdventOfCode)

  import ExProf.Macro

  @shortdoc "Day 21 Part 1"
  def run(args) do
    input = AdventOfCode.Input.get!(21, Keyword.get(config(), :year))

    do_work = fn ->
      if Enum.member?(args, "-b"),
         do: Benchee.run(%{part_1: fn -> input |> part1() end}),
         else:
           input
           |> part1()
           |> IO.inspect(label: "Part 1 Results")
    end

    if Enum.member?(args, "-p") do
      profile do
        do_work.()
      end
    else
      do_work.()
    end
  end
end

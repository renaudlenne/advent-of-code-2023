defmodule Mix.Tasks.D16.P2 do
  use Mix.Task

  import AdventOfCode.Day16
  defp config, do: Application.get_env(:advent_of_code, AdventOfCode)

  @shortdoc "Day 16 Part 2"
  def run(args) do
    input = AdventOfCode.Input.get!(16, Keyword.get(config(), :year))

    if Enum.member?(args, "-b"),
      do: Benchee.run(%{part_2: fn -> input |> part2() end}, warmup: 10, time: 30),
      else:
        input
        |> part2()
        |> IO.inspect(label: "Part 2 Results")
  end
end

defmodule Mix.Tasks.D10.P1 do
  use Mix.Task

  import AdventOfCode.Day10
  defp config, do: Application.get_env(:advent_of_code, AdventOfCode)

  @shortdoc "Day 10 Part 1"
  def run(args) do
    input = AdventOfCode.Input.get!(10, Keyword.get(config(), :year))

    if Enum.member?(args, "-b"),
      do: Benchee.run(%{part_1: fn -> input |> part1() end}),
      else:
        input
        |> part1()
        |> IO.inspect(label: "Part 1 Results")
  end
end

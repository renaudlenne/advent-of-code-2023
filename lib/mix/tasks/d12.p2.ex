defmodule Mix.Tasks.D12.P2 do
  use Mix.Task

  import AdventOfCode.Day12
  defp config, do: Application.get_env(:advent_of_code, AdventOfCode)

  @shortdoc "Day 12 Part 2"
  def run(args) do
    input = AdventOfCode.Input.get!(12, Keyword.get(config(), :year))

    if Enum.member?(args, "-b"),
      do: Benchee.run(%{part_2: fn -> input |> part2() end}, warmup: 10, time: 30),
      else:
        input
        |> part2()
        |> IO.inspect(label: "Part 2 Results")
  end
end
